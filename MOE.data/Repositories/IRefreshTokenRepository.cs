﻿using MOE.data.Entities;

namespace MOE.data.Repositories;

public interface IRefreshTokenRepository : IBaseRepository<RefreshToken>
{
    Task<RefreshToken> GetTokenAsync(string refreshToken);
}