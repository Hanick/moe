﻿using MOE.data.Entities;

namespace MOE.data.Repositories;

public interface IStripeRepository : IBaseRepository<StripeSubscription>
{
    Task<string> GetSubscriptionIdAsync(string? subscriptionName);
}