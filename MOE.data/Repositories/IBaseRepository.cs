﻿namespace MOE.data.Repositories;

public interface IBaseRepository<TEntity> where TEntity : class
{
    IQueryable<TEntity> GetAll();
    Task<bool> SaveAsync();
    Task<bool> CreateAsync(TEntity entity);
    Task<bool> UpdateAsync(TEntity entity);
    Task<bool> UpdateRangeAsync(TEntity entity);
    Task<TEntity> FindByIdAsync(Guid id);
    Task SoftDeleteAllEntitiesASync(bool isDelete, DateTime dateTime);
    Task<bool> SoftDeleteByIdAsync(Guid id, bool isDelete, DateTime dateTime);
}

