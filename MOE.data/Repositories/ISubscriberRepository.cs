﻿using MOE.data.Entities;

namespace MOE.data.Repositories;

public interface ISubscriberRepository : IBaseRepository<Subscriber>
{
    Task<Subscriber> GetSubByIdAsync(string? id);
    Task<Subscriber> UpdateSubscriberAsync(Subscriber subscription);
    Task<Subscriber> CreateSubscriberAsync(Subscriber subscription);
    string GetSubscriptionId(string customerId);
}