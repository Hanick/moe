﻿using MOE.data.Entities;

namespace MOE.data.Repositories;

public interface IExpenseRepository : IBaseRepository<Expense>
{
    Task<Expense> GetExpenseByIdAsync(Guid expenseId);
    Task<bool> CreateExpenseAsync(Expense expense);
    Task<bool> UpdateExpenseAsync(Expense expense);
    Task<bool> UserOwnExpenseAsync(Guid expenseId, Guid userId);
    IQueryable<Expense> GetAllUserExpenses(Guid userId, int from, int to);
    IQueryable<Expense> GetAllUserExpensesByUserId(Guid userId);
}