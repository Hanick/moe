﻿using Microsoft.EntityFrameworkCore;
using MOE.data.Entities;

namespace MOE.data.Repositories.Impl;

public class RefreshTokenRepository : BaseRepository<RefreshToken>, IRefreshTokenRepository
{
    public RefreshTokenRepository(DataContext context) : base(context)
    {
    }

    public async Task<RefreshToken> GetTokenAsync(string refreshToken)
    {
        return await GetAll().SingleOrDefaultAsync(x => x.Id.ToString() == refreshToken);
    }
}