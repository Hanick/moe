﻿using Microsoft.EntityFrameworkCore;
using MOE.data.Entities;

namespace MOE.data.Repositories.Impl;

public class SubscriberRepository : BaseRepository<Subscriber>, ISubscriberRepository
{
    public SubscriberRepository(DataContext context) : base(context)
    {
    }

    public async Task<Subscriber> GetSubByIdAsync(string? id)
    {
        return await GetAll().SingleOrDefaultAsync(x => x.CustomerId == id);
    }

    public async Task<Subscriber> UpdateSubscriberAsync(Subscriber subscription)
    {
        await UpdateRangeAsync(subscription);
        return subscription;
    }

    public async Task<Subscriber> CreateSubscriberAsync(Subscriber subscription)
    {
        await CreateAsync(subscription);
        return subscription;
    }

    public string GetSubscriptionId(string customerId)
    {
        return GetAll().SingleOrDefault(x => x.CustomerId == customerId).SubscriptionId;
    }
}

