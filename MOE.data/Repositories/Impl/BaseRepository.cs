﻿using Microsoft.EntityFrameworkCore;
using MOE.data.Entities;

namespace MOE.data.Repositories.Impl;

public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
{
    private readonly DataContext _context;

    public BaseRepository(DataContext context)
    {
        _context = context;
    }

    public IQueryable<TEntity> GetAll()
    {
        return _context.Set<TEntity>().Where(x => !x.IsDeleted);
    }

    public async Task<bool> SaveAsync()
    {
        var saved = await _context.SaveChangesAsync();
        return saved > 0;
    }

    public async Task<bool> CreateAsync(TEntity entity)
    {
        await _context.Set<TEntity>().AddAsync(entity);
        return await SaveAsync();
    }

    public async Task<bool> UpdateAsync(TEntity entity)
    {
        _context.Set<TEntity>().Update(entity);
        return await SaveAsync();
    }

    public async Task<bool> UpdateRangeAsync(TEntity entity)
    {
        _context.Set<TEntity>().UpdateRange(entity);
        return await SaveAsync();
    }

    public async Task<TEntity> FindByIdAsync(Guid id)
    {
        return await _context.Set<TEntity>().FindAsync(id);

    }

    public async Task SoftDeleteAllEntitiesASync(bool isDelete, DateTime dateTime)
    {
        var item = await _context.Set<TEntity>().Where(x => x.IsDeleted != isDelete).ToListAsync();

        if (item.Count == 0)
            return;

        foreach (var itm in item)
        {
            await SoftDeleteByIdAsync(itm.Id, isDelete, dateTime);
        }
    }

    public async Task<bool> SoftDeleteByIdAsync(Guid id, bool isDelete, DateTime dateTime)
    {
        var item = await FindByIdAsync(id);
        item.IsDeleted = isDelete;
        item.DateOfDeleting = dateTime;
        return await UpdateAsync(item);
    }
}