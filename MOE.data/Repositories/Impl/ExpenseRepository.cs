﻿using Microsoft.EntityFrameworkCore;
using MOE.data.Entities;

namespace MOE.data.Repositories.Impl;

public class ExpenseRepository : BaseRepository<Expense>, IExpenseRepository
{
    public ExpenseRepository(DataContext context) : base(context)
    {
    }

    public async Task<Expense> GetExpenseByIdAsync(Guid expenseId)
    {
        return await GetAll().SingleOrDefaultAsync(x => x.Id == expenseId);
    }

    public async Task<bool> CreateExpenseAsync(Expense expense)
    {
        return await CreateAsync(expense);
    }

    public async Task<bool> UpdateExpenseAsync(Expense expense)
    {
        return await UpdateAsync(expense);
    }

    public async Task<bool> UserOwnExpenseAsync(Guid expenseId, Guid userId)
    {
        var post = await GetAll().AsNoTracking().SingleOrDefaultAsync(x => x.Id == expenseId);

        return post != null && post.UserKey == userId;
    }

    public IQueryable<Expense> GetAllUserExpenses(Guid userId, int from, int to)
    {
        return GetAll().Where(x => x.UserKey == userId && 
                                   x.DateOfPurchase.Month >= from && x.DateOfPurchase.Month <= to);
    }

    public IQueryable<Expense> GetAllUserExpensesByUserId(Guid userId)
    {
        return GetAll().Where(x => x.UserKey == userId);
    }
}