﻿using Microsoft.EntityFrameworkCore;
using MOE.data.Entities;

namespace MOE.data.Repositories.Impl;

public class StripeRepository : BaseRepository<StripeSubscription>, IStripeRepository
{
    public StripeRepository(DataContext context) : base(context)
    {
    }

    public async Task<string> GetSubscriptionIdAsync(string? subscriptionName)
    {
        var subName =
            await GetAll().FirstOrDefaultAsync(x => x.SubscriptionName.ToLower() == subscriptionName.ToLower());

        return subName == null ? "Not found" : subName.SubscriptionId;
    }
}