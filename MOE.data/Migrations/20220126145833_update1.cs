﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MOE.data.Migrations
{
    public partial class update1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscribers_Users_UserKey",
                table: "Subscribers");

            migrationBuilder.DropIndex(
                name: "IX_Subscribers_UserKey",
                table: "Subscribers");

            migrationBuilder.DropColumn(
                name: "UserKey",
                table: "Subscribers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserKey",
                table: "Subscribers",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Subscribers_UserKey",
                table: "Subscribers",
                column: "UserKey");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscribers_Users_UserKey",
                table: "Subscribers",
                column: "UserKey",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
