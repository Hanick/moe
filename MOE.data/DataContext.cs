﻿using Microsoft.EntityFrameworkCore;
using MOE.data.Entities;

namespace MOE.data;

public class DataContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Expense> Expenses { get; set; }
    public DbSet<StripeSubscription> StripeSubscriptions { get; set; }
    public DbSet<Subscriber> Subscribers { get; set; }
    public DbSet<RefreshToken> RefreshTokens { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Expense>()
            .HasOne(c => c.User)
            .WithMany()
            .HasForeignKey(p => p.UserKey);
    }

    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }
}