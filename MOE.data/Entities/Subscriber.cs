﻿namespace MOE.data.Entities;

public class Subscriber : BaseEntity
{
    public string? Status { get; set; }
    public string? CustomerId { get; set; }
    public string? SubscriptionId { get; set; }
    public DateTime CurrentPeriodEnd { get; set; }
}