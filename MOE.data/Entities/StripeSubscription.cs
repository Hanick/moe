﻿namespace MOE.data.Entities;

public class StripeSubscription : BaseEntity
{
    public string? SubscriptionName { get; set; }
    public string? SubscriptionId { get; set; }
    public decimal SubscriptionPrice { get; set; }
}