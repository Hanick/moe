﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MOE.data.Entities
    ;
public class RefreshToken : BaseEntity
{
    public string? JwtId { get; set; }
    public DateTime CreationDate { get; set; }
    public DateTime ExpiryDate { get; set; }
    public bool Used { get; set; }
    public bool Invalidated { get; set; }
    public Guid UserKey { get; set; }
    [JsonIgnore]
    [ForeignKey("UserKey")]
    public User? User { get; set; }
}