﻿using Microsoft.AspNetCore.Identity;

namespace MOE.data.Entities;

public class User : IdentityUser<Guid>
{
    public override string? UserName { get; set; }
    public override string? PasswordHash { get; set; }
    public override string? Email { get; set; }
    public override string? PhoneNumber { get; set; }
    public string? CustomerId { get; set; }
    public long? TelegramChatId { get; set; }
    public string? LastTelegramCommand { get; set; }
    public bool IsDeleted { get; set; }
    public DateTime DateOfDeleting { get; set; }
}