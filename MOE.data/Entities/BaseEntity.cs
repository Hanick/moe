﻿using System.ComponentModel.DataAnnotations;

namespace MOE.data.Entities;

public class  BaseEntity
{
    [Key]
    public Guid Id { get; set; }
    public bool IsDeleted { get; set; }
    public DateTime DateOfDeleting { get; set; }
}