﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MOE.data.Entities;

public class Expense : BaseEntity
{
    public string? Check { get; set; }
    public decimal TotalPrice { get; set; }
    public DateTime DateOfPurchase { get; set; }
    public Guid UserKey { get; set; }
    [JsonIgnore]
    [ForeignKey("UserKey")]
    public User? User { get; set; }
}