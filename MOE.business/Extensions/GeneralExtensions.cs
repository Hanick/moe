﻿using Microsoft.AspNetCore.Http;
using MOE.sharing.DTOs.Models;
using MOE.sharing.DTOs.Responses;

namespace MOE.business.Extensions;

public static class GeneralExtensions
{
    public static string GetUserId(this HttpContext httpContext)
    {
        return httpContext.User == null ? string.Empty : httpContext.User.Claims.FirstOrDefault(x => x.Type == "id").Value;
    }

    public static StatisticResponse<IQueryable<T>> ToPagedResponse<T>(this IQueryable<T> pagedData,
        PagingModel validFilter, decimal totalPrice)
    {
        var response = new StatisticResponse<IQueryable<T>>(pagedData.Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
            .Take(validFilter.PageSize), validFilter.PageNumber, validFilter.PageSize);
        var totalPages = pagedData.Count() / (double)validFilter.PageSize;
        var roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));

        response.NumberOfPurchases = pagedData.Count();
        response.TotalAmount = totalPrice;
        response.TotalPages = roundedTotalPages;
        response.TotalRecords = response.NumberOfPurchases;
        response.Success = true;

        return response;
    }
}