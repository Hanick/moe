﻿using MOE.business.Services;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;

public class AddExpenseCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IUserService _userService;
    public AddExpenseCommand(IBotService botService, IUserService userService)
    {
        _userService = userService;
        _telegramBotClient = botService.GetBot().Result;
    }

    public override string Name => CommandNames.AddExpense;
    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.AddExpense);

        const string message = "To record an expense, please, enter the data in this format: \n" +
                               "Check number : Check cost : Date of Purchase \n" +
                               "(Date Format: 01.01.2000)";

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message);

    }
}