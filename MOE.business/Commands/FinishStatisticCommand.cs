﻿using MOE.business.Services;
using MOE.sharing.DTOs.Requests;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;

public class FinishStatisticCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IUserService _userService;
    private readonly IStatisticService _statisticService;

    public FinishStatisticCommand(IBotService botService, IUserService userService,
        IStatisticService statisticService)
    {
        _userService = userService;
        _statisticService = statisticService;
        _telegramBotClient = botService.GetBot().Result;
    }

    public override string Name => CommandNames.FinishStatisticCommand;

    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.FinishStatisticCommand);
        var userData = update.Message.Text.Split(':');

        var from = int.Parse(userData[0]);
        var to = int.Parse(userData[1]);

        if (from < 1 && from > 31 && to < 1 && to > 31)
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "Wrong number of month");
            return;
        }

        var req = new StatisticRequest
        {
            From = from,
            To = to
        };
        var statistic = _statisticService.GetStatistic(req, user.Id);

        var message = new StringBuilder($"Here your statistic \n" +
                                        $"Total price: {statistic.TotalAmount}\n" +
                                        $"Total operations: {statistic.TotalRecords}\n" +
                                        $"Your expenses:\n");

        foreach (var data in statistic.Data)
        {
            message.AppendLine($"Check = {data.Check}: \t TotalPrice = {data.TotalPrice}\t DateOfPurchase = {data.DateOfPurchase.ToShortDateString()}");
        }

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message.ToString());
    }
}