﻿using Microsoft.IdentityModel.Tokens;
using MOE.business.Services;
using MOE.data.Entities;
using MOE.data.Repositories;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace MOE.business.Commands;

public class FinishExpensesCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IExpenseRepository _expenseRepository;
    private readonly IUserService _userService;
    private readonly DateTime _defaultDateTime;

    public FinishExpensesCommand(IBotService botService, IExpenseRepository expenseRepository, IUserService userService)
    {
        _expenseRepository = expenseRepository;
        _userService = userService;
        _telegramBotClient = botService.GetBot().Result;
    }
    public override string Name => CommandNames.FinishExpenses;
    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.FinishExpenses);
        var userData = update.Message?.Text?.Split(':');

        var check = userData[0];
        if (check.IsNullOrEmpty())
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "You have not entered name of check");
            return;
        }

        var totalPrice = decimal.Parse(userData[1]);
        if (totalPrice <= 0)
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "You have entered invalid check price");
            return;
        }

        var dateOfPurchase = DateTime.Parse(userData[2]);
        if (dateOfPurchase == _defaultDateTime && dateOfPurchase < DateTime.UtcNow)
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId,
                "You have entered invalid date of purchase");
            return;
        }

        var expense = new Expense
        {
            Check = check,
            TotalPrice = totalPrice,
            DateOfPurchase = dateOfPurchase,
            UserKey = user.Id
        };

        await _expenseRepository.CreateExpenseAsync(expense);

        var message = new StringBuilder("We have successfully record your expense!");

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message.ToString(), ParseMode.Markdown);
    }
}