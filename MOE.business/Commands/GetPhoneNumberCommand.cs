﻿using MOE.business.Services;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;

public class GetPhoneNumberCommand : BaseCommand
{
    private readonly IUserService _userService;
    private readonly TelegramBotClient _telegramBotClient;

    public GetPhoneNumberCommand(IUserService userService, IBotService botService)
    {
        _userService = userService; ;
        _telegramBotClient = botService.GetBot().Result;
    }

    public override string Name => CommandNames.GetPhoneNumber;

    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.GetPhoneNumber);

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "Thanks for providing this information, now you have full access to the program");
    }
}