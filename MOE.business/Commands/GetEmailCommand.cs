﻿using MOE.business.Services;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;
public class GetEmailCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IUserService _userService;

    public GetEmailCommand(IBotService botService, IUserService userService)
    {
        _userService = userService;
        _telegramBotClient = botService.GetBot().Result;
    }

    public override string Name => CommandNames.GetEmail;

    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.GetEmail);

        var isSuccessful = await _userService.UpdateEmail(update.Message.Text, update.Message.Chat.Username);

        if (!isSuccessful)
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId,
                "Something went wrong, please try again later");
            return;
        }

        var message = new StringBuilder($"Email {user.Email} successfully added, now you need choose the subscription plan: \n\n" +
                                "\t type: \n standart \t Subscription for 1 month, price 10$ \n" +
                                "\t type: \n advanced \t Subscription for 3 month, price 20$ \n" +
                                "\t type: \n pro \t Subscription for 6 month, price 30$ \n");

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message.ToString());
    }
}