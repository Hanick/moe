﻿using MOE.business.Services;
using MOE.data.Repositories;
using MOE.sharing.DTOs.Requests;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;

public class FinishGetLinkCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IUserService _userService;
    private readonly IPaymentsService _paymentsService;
    private readonly IStripeRepository _stripeRepository;

    public FinishGetLinkCommand(IBotService botService, IUserService userService,
        IPaymentsService paymentsService, IStripeRepository stripeRepository)
    {
        _userService = userService;
        _paymentsService = paymentsService;
        _stripeRepository = stripeRepository;
        _telegramBotClient = botService.GetBot().Result;
    }

    public override string Name => CommandNames.FinishGetLinkForPurchase;

    public override async Task ExecuteAsync(Update update)
    {
        var subscription = await _stripeRepository.GetSubscriptionIdAsync(update.Message.Text);
        var user = await _userService.GetOrCreateAsync(update, CommandNames.FinishGetLinkForPurchase);

        if (subscription == "Not found")
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "Name is invalid or not exist, please enter correct subscription name");
            return;
        }

        var url = new CheckoutSessionRequest
        {
            PriceId = subscription
        };

        var message = new StringBuilder($"Here your link for purchase \n" +
                                        $"{_paymentsService.CreateCheckoutSession(url).Result}");

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message.ToString());
    }
}