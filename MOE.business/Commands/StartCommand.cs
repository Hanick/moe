﻿using MOE.business.Services;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;

namespace MOE.business.Commands;
public class StartCommand : BaseCommand
{
    private readonly IUserService _userService;
    private readonly TelegramBotClient _telegramBotClient;

    public StartCommand(IUserService userService, IBotService botService)
    {
        _userService = userService;
        _telegramBotClient = botService.GetBot().Result;
    }

    public override string Name => CommandNames.StartCommand;

    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.StartCommand);

        if (user.IsDeleted)
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "Account was deleted, try another phone number or contact to our support team");
            return;
        }

        var inlineKeyboard = new ReplyKeyboardMarkup(new[]
        {
            new []
            {
                KeyboardButton.WithRequestContact("Get your phone number"),
                new KeyboardButton("Go next")
            }
        });

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId,
            "Welcome, here you can record and get the statistic about your expenses, to start, get your phone number (if you do it previously, just press Go next). After press Go Next",
            ParseMode.Markdown, replyMarkup : inlineKeyboard);
    }
}