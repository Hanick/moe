﻿using MOE.business.Services;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;

public class GetStatisticCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IUserService _userService;

    public GetStatisticCommand(IBotService botService, IUserService userService)
    {
        _telegramBotClient = botService.GetBot().Result;
        _userService = userService;
    }

    public override string Name => CommandNames.GetStatistic;
    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.GetStatistic);

        const string message = ("To get statistic, please, enter next data in format: \n" +
                                "From: To (Number of Month)");

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message);
    }
}