﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MOE.business.Services;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace MOE.business.Commands;

public class NextCommand : BaseCommand
{
    private readonly IUserService _userService;
    private readonly TelegramBotClient _telegramBotClient;
    private readonly UserManager<data.Entities.User> _userManager;

    public NextCommand(IUserService userService, IBotService botService, UserManager<data.Entities.User> userManager)
    {
        _userService = userService;
        _userManager = userManager;
        _telegramBotClient = botService.GetBot().Result;
    }
    public override string Name => CommandNames.NextCommand;

    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.NextCommand);

        if (user.IsDeleted)
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "Account was deleted, try another phone number or contact to our support team");
            return;
        }

        var checkPhoneNumber = await _userManager.Users.FirstOrDefaultAsync(x => x.TelegramChatId == user.TelegramChatId);

        if (checkPhoneNumber.PhoneNumber.IsNullOrEmpty())
        {
            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "You need provide phone number first, to get full access to the program");
            return;
        }

        var inlineKeyboard = new InlineKeyboardMarkup(new[]
            {
                new[]
                {
                    new InlineKeyboardButton("Get all expenses")
                    {
                        CallbackData = CommandNames.GetExpenses
                    },
                    new InlineKeyboardButton("Create an expense")
                    {
                        CallbackData = CommandNames.AddExpense
                    }
                },
                new[]
                {
                    new InlineKeyboardButton("Get statistic")
                    {
                        CallbackData = CommandNames.GetStatistic
                    },
                    new InlineKeyboardButton("Get premium plan")
                    {
                        CallbackData = CommandNames.GetLinkForPurchase
                    }
                }
            });

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId,
            $"Welcome {user.UserName}!",
            replyMarkup: inlineKeyboard);
    }
}