﻿using MOE.business.Services;
using MOE.data.Repositories;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;

public class GetExpensesCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IUserService _userService;
    private readonly IExpenseRepository _expenseRepository;
    public GetExpensesCommand(IBotService botService, IUserService userService, IExpenseRepository expenseRepository)
    {
        _telegramBotClient = _telegramBotClient = botService.GetBot().Result;
        _userService = userService;
        _expenseRepository = expenseRepository;
    }

    public override string Name => CommandNames.GetExpenses;
    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.GetExpenses);

        var expenses = _expenseRepository.GetAllUserExpensesByUserId(user.Id);

        if (!expenses.Any())
        {
            var errorMessage = new StringBuilder();

            await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, "You don't have any records");
            return;
        }

        var message = new StringBuilder("Here your expenses: \n");

        foreach (var exp in expenses)
        {
            message.AppendLine($"Check = {exp.Check}: \t TotalPrice = {exp.TotalPrice}\t DateOfPurchase = {exp.DateOfPurchase.ToShortDateString()}");
        }

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message.ToString());
    }
}