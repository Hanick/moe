﻿using MOE.business.Services;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MOE.business.Commands;

public class GetLinkForPurchaseCommand : BaseCommand
{
    private readonly TelegramBotClient _telegramBotClient;
    private readonly IUserService _userService;

    public GetLinkForPurchaseCommand(IBotService botService, IUserService userService)
    {
        _userService = userService;
        _telegramBotClient = botService.GetBot().Result;
    }

    public override string Name => CommandNames.GetLinkForPurchase;

    public override async Task ExecuteAsync(Update update)
    {
        var user = await _userService.GetOrCreateAsync(update, CommandNames.GetLinkForPurchase);

        const string message =
            "With pro plan, you will be have opportunity, to view statistic by your selected period of time. \n\n" +
            "First of all you need to provide your email (This email you need to enter when you will be purchase)";

        await _telegramBotClient.SendTextMessageAsync(user.TelegramChatId, message);
    }
}