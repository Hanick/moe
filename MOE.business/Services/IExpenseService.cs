﻿using MOE.data.Entities;

namespace MOE.business.Services;

public interface IExpenseService
{
    IQueryable<Expense> GetPosts();
    Task<Expense> GetPostsByIdAsync(Guid expenseId);
    Task<bool> CreateAsync(Expense postExpense);
    Task<bool> UpdateAsync(Expense postExpense);
    Task<bool> DeleteAsync(Guid expenseId);
    Task<bool> UserOwnsExpenseAsync(Guid expenseId, Guid getUserId);
}