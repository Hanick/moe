﻿namespace MOE.business.Services;

public interface IEntitiesService
{
    Task SoftDeleteAllEntities(bool isDelete, DateTime dateTime);
}