﻿using Telegram.Bot.Types;
using User = MOE.data.Entities.User;

namespace MOE.business.Services;

public interface IUserService
{
    Task<User> GetOrCreateAsync(Update update, string command);
    Task<bool> UpdateEmail(string email, string nickName);
}