﻿using Telegram.Bot;

namespace MOE.business.Services;

public interface IBotService
{
    Task<TelegramBotClient> GetBot();
}