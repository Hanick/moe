﻿using MOE.data.Entities;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;

namespace MOE.business.Services;

public interface IStatisticService
{
    StatisticResponse<IQueryable<Expense>> GetStatistic(StatisticRequest statisticRequest, Guid userId);
}