﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MOE.data.Entities;
using MOE.data.Repositories;

namespace MOE.business.Services.Impl;

public class EntitiesService : IEntitiesService
{
    private readonly IExpenseRepository _expenseRepository;
    private readonly ISubscriberRepository _subscriberRepository;
    private readonly IStripeRepository _stripeRepository;
    private readonly IRefreshTokenRepository _refreshTokenRepository;
    private readonly UserManager<User> _userManager;


    public EntitiesService(IExpenseRepository expenseRepository, ISubscriberRepository subscriberRepository, IStripeRepository stripeRepository, UserManager<User> userManager, IRefreshTokenRepository refreshTokenRepository)
    {
        _expenseRepository = expenseRepository;
        _subscriberRepository = subscriberRepository;
        _stripeRepository = stripeRepository;
        _userManager = userManager;
        _refreshTokenRepository = refreshTokenRepository;
    }

    public async Task SoftDeleteAllEntities(bool isDelete, DateTime dateTime)
    {
        await _expenseRepository.SoftDeleteAllEntitiesASync(isDelete, dateTime);
        await _stripeRepository.SoftDeleteAllEntitiesASync(isDelete, dateTime);
        await _subscriberRepository.SoftDeleteAllEntitiesASync(isDelete, dateTime);
        await _refreshTokenRepository.SoftDeleteAllEntitiesASync(isDelete, dateTime);
        await SoftDeleteAllUsersAsync(isDelete, dateTime);
    }

    private async Task SoftDeleteAllUsersAsync(bool isDelete, DateTime dateTime)
    {
        var user = await _userManager.Users.Where(x => x.IsDeleted != isDelete).ToListAsync();

        if (user.Count == 0)
            return;

        foreach (var usr in user)
        {
            var item = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == usr.Id);
            item.IsDeleted = isDelete;
            item.DateOfDeleting = dateTime;
            await _userManager.UpdateAsync(item);
        }
    }
}