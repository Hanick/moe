﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using MOE.sharing.DTOs.Domain;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.Options;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using MOE.data.Entities;
using MOE.data.Repositories;

namespace MOE.business.Services.Impl;

public class IdentityService : IIdentityService
{
    private readonly UserManager<User> _userManager;
    private readonly JwtSettings _jwtSettings;
    private readonly IMapper _mapper;
    private readonly TokenValidationParameters _tokenValidationParameters;
    private readonly IRefreshTokenRepository _refreshTokenRepository;

    public IdentityService(JwtSettings jwtSettings, UserManager<User> userManager, IMapper mapper, TokenValidationParameters tokenValidationParameters, IRefreshTokenRepository refreshTokenRepository)
    {
        _jwtSettings = jwtSettings;
        _userManager = userManager;
        _mapper = mapper;
        _tokenValidationParameters = tokenValidationParameters;
        _refreshTokenRepository = refreshTokenRepository;
    }

    public async Task<AuthenticationResult> RegisterAsync(RegistrationRequest request)
    {
        var checkUser = await _userManager.FindByEmailAsync(request.Email);

        if (checkUser != null)
            return new AuthenticationResult
            {
                Errors = new[] { "User already exist" }
            };

        var newUser = _mapper.Map<User>(request);

        var createdUser = await _userManager.CreateAsync(newUser, request.Password);

        if (!createdUser.Succeeded)
            return new AuthenticationResult
            {
                Errors = createdUser.Errors.Select(x => x.Description)
            };

        return await GenerateAuthResultForUserAsync(newUser);
    }

    public async Task<AuthenticationResult> LoginAsync(AuthenticationRequest request)
    {
        var user = await _userManager.FindByEmailAsync(request.Email);

        if (user == null)
            return new AuthenticationResult
            {
                Errors = new[] { "User does not exist" }
            };

        var checkValidPassword = await _userManager.CheckPasswordAsync(user, request.Password);

        if (!checkValidPassword || user.IsDeleted)
            return new AuthenticationResult
            {
                Errors = new[] { "Password or Email is incorrect" }
            };

        return await GenerateAuthResultForUserAsync(user);
    }

    public async Task<AuthenticationResult> RefreshTokenASync(string token, string refreshToken)
    {
        var validatedToken = GetPrincipalFromToken(token);

        if (validatedToken == null)
            return new AuthenticationResult { Errors = new []{"Invalid token"}};

        var expiryDateUnix =
            long.Parse(validatedToken.Claims.SingleOrDefault(x => x.Type == JwtRegisteredClaimNames.Exp).Value);

        var expiryDateTimeUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            .AddSeconds(expiryDateUnix);

        if (expiryDateTimeUtc > DateTime.UtcNow)
            return new AuthenticationResult { Errors = new[] {"This token hasn't expired yet"}};

        var jti = validatedToken.Claims.SingleOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

        var storedRefreshToken = await _refreshTokenRepository.GetTokenAsync(refreshToken);

        if (storedRefreshToken == null)
            return new AuthenticationResult { Errors = new[] {"This refresh token doesn't exist"}};

        if (DateTime.UtcNow > storedRefreshToken.ExpiryDate)
            return new AuthenticationResult { Errors = new[] { "This refresh token has expired"}};

        if (storedRefreshToken.Invalidated)
            return new AuthenticationResult { Errors = new[] { "This refresh token has been invalidated"}};

        if (storedRefreshToken.Used)
            return new AuthenticationResult { Errors = new[] { "This refresh token has been used"}};

        if (storedRefreshToken.JwtId != jti)
            return new AuthenticationResult { Errors = new[] { "This refresh token doesn't match this JWT"}};

        storedRefreshToken.Used = true;
        await _refreshTokenRepository.UpdateAsync(storedRefreshToken);

        var user = await _userManager.FindByIdAsync(validatedToken.Claims.SingleOrDefault(x => x.Type == "id").Value);
        return await GenerateAuthResultForUserAsync(user);
    }

    private ClaimsPrincipal GetPrincipalFromToken(string token)
    {
        var tokenHendler = new JwtSecurityTokenHandler();

        try
        {
            var principal = tokenHendler.ValidateToken(token, _tokenValidationParameters, out var validatedToken);
            if (!IsJwtWithValidSecurityAlgorithm(validatedToken))
                return null;

            return principal;
        }
        catch
        {
            return null;
        }
    }

    private bool IsJwtWithValidSecurityAlgorithm(SecurityToken validatedToken)
    {
        return (validatedToken is JwtSecurityToken jwtSecurityToken) &&
               jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                   StringComparison.InvariantCultureIgnoreCase);
    }

    private async Task<AuthenticationResult> GenerateAuthResultForUserAsync(User user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim("id", user.Id.ToString())
            }),
            Expires = DateTime.UtcNow.Add(_jwtSettings.TokenLifetime),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);

        var refreshToken = new RefreshToken
        {
            JwtId = token.Id,
            UserKey = user.Id,
            CreationDate = DateTime.UtcNow,
            ExpiryDate = DateTime.UtcNow.AddMonths(6)
        };

        await _refreshTokenRepository.CreateAsync(refreshToken);

        return new AuthenticationResult
        {
            Success = true,
            Token = tokenHandler.WriteToken(token),
            RefreshToken = refreshToken.Id.ToString()
        };
    }
}

