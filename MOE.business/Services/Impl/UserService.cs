﻿using Microsoft.AspNetCore.Identity;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using User = MOE.data.Entities.User;

namespace MOE.business.Services.Impl;

public class UserService : IUserService
{
    private readonly UserManager<User> _userManager;
    public UserService(UserManager<User> userManager)
    {
        _userManager = userManager;
    }

    public async Task<User> GetOrCreateAsync(Update update, string command)
    {
        var newUser = update.Type switch
        {
            UpdateType.CallbackQuery => new User

            {
                UserName = update.CallbackQuery?.From.Username,
                TelegramChatId = update.CallbackQuery?.Message?.Chat.Id
            },
            UpdateType.Message => new User
            {
                UserName = update.Message?.Chat.Username,
                TelegramChatId = update.Message?.Chat.Id,
                PhoneNumber = update.Message?.Contact?.PhoneNumber
            }
        };

        var currentUser = await _userManager.FindByNameAsync(newUser.UserName);

        if (currentUser != null)
        {
            currentUser.LastTelegramCommand = command;
            await _userManager.UpdateAsync(currentUser);
        }

        if (currentUser != null && update.Message?.Contact?.PhoneNumber != null)
        {
            currentUser.PhoneNumber = newUser.PhoneNumber;
            await _userManager.UpdateAsync(currentUser);
        }

        if (currentUser != null) return currentUser;

        await _userManager.CreateAsync(newUser);

        return newUser;
    }

    public async Task<bool> UpdateEmail(string email, string nickName)
    {
        var user = await _userManager.FindByNameAsync(nickName);

        var currentUser = new User
        {
            Email = email
        };
        user.Email = currentUser.Email;
        await _userManager.UpdateAsync(user);
        return true;
    }
}