﻿using Microsoft.AspNetCore.Identity;
using MOE.data.Entities;
using MOE.data.Repositories;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;
using MOE.sharing.Options;
using Stripe;
using Stripe.Checkout;

namespace MOE.business.Services.Impl;

public class PaymentsService : IPaymentsService
{
    private readonly StripeSettings _stripeSettings;
    private readonly UserManager<User> _userManager;
    private readonly ISubscriberRepository _subscriberRepository;

    public PaymentsService(StripeSettings stripeSettings, UserManager<User> userManager, ISubscriberRepository subscriberRepository)
    {
        _stripeSettings = stripeSettings;
        _userManager = userManager;
        _subscriberRepository = subscriberRepository;
    }

    public async Task<CheckoutSessionResponse> CreateCheckoutSession(CheckoutSessionRequest req)
    {
        StripeConfiguration.ApiKey = _stripeSettings.PrivateKey;

        const string domain = "http://localhost:7263";

        var options = new SessionCreateOptions
        {
            SuccessUrl = domain + "/success.html",
            CancelUrl = domain + "/cancel.html",
            PaymentMethodTypes = new List<string>
            {
                "card",
            },
            Mode = "subscription",
            LineItems = new List<SessionLineItemOptions>
            {
                new SessionLineItemOptions
                {
                    Price = req.PriceId,
                    Quantity = 1,
                },
            },
        };

        var service = new SessionService();
        await service.CreateAsync(options);

        var session = await service.CreateAsync(options);

        return new CheckoutSessionResponse
        {
            SessionUrl = session.Url,
        };
    }

    public async Task AddSubscriptionToDb(Subscription subscription)
    {
        try
        {
            var subscriber = new Subscriber
            {
                SubscriptionId = subscription.Id,
                CustomerId = subscription.CustomerId,
                Status = "active",
                CurrentPeriodEnd = subscription.CurrentPeriodEnd
            };
            await _subscriberRepository.CreateSubscriberAsync(subscriber);
        }
        catch (Exception e)
        {
            Console.WriteLine("Unable to add new subscriber to Database");
            Console.WriteLine(e.Message);
        }
    }

    public async Task GetCustomerIdAndAddToDb(Customer customer)
    {
        try
        {
            var updUser = await _userManager.FindByEmailAsync(customer.Email);

            if (updUser != null)
            {
                updUser.CustomerId = customer.Id;
                await _userManager.UpdateAsync(updUser);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Unable to add customer id to user");
            Console.WriteLine(e);
        }
    }

    public async Task UpdateSubscription(Subscription subscription)
    {
        try
        {
            var updSub = await _subscriberRepository.GetSubByIdAsync(subscription.Id);
            if (updSub != null)
            {
                updSub.Status = subscription.Status;
                updSub.CurrentPeriodEnd = subscription.CurrentPeriodEnd;
                updSub.SubscriptionId = subscription.Id;
                updSub.CustomerId = subscription.CustomerId;

                await _subscriberRepository.UpdateSubscriberAsync(updSub);
                Console.WriteLine("Subscription Updated");
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine("Unable to update subscription");
        }
    }
}

