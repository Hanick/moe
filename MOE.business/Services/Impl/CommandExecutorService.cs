﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MOE.business.Commands;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using User = MOE.data.Entities.User;

namespace MOE.business.Services.Impl;

public class CommandExecutorService : ICommandExecutorService
{
    private readonly List<BaseCommand> _baseCommands;
    private BaseCommand _lastCommand;
    private readonly UserManager<User> _userManager;

    public CommandExecutorService(IServiceProvider serviceProvider, BaseCommand lastCommand,
        UserManager<User> userManager)
    {
        _lastCommand = lastCommand;
        _userManager = userManager;
        _baseCommands = serviceProvider.GetServices<BaseCommand>().ToList();
    }

    public async Task Execute(Update update)
    {

        if (update.Message?.Chat == null && update.CallbackQuery == null)
            return;

        if (update.Message?.Contact != null)
        {
            await ExecuteCommand(CommandNames.GetPhoneNumber, update);
            return;
        }

        if (update.Type == UpdateType.Message)
        {
            switch (update.Message?.Text)
            {
                case "Go next":
                    {
                        await ExecuteCommand(CommandNames.NextCommand, update);
                        return;
                    }
                case CommandNames.StartCommand:
                    {
                        await ExecuteCommand(CommandNames.StartCommand, update);
                        return;
                    }
                case null:
                    {
                        await ExecuteCommand(CommandNames.NextCommand, update);
                        break;
                    }
            }
        }

        if (update.Type == UpdateType.CallbackQuery)
        {
            await ExecuteCommand(update.CallbackQuery?.Data, update);
            return;
        }

        var lastUserCommand = await _userManager.FindByNameAsync(update.Message.Chat.Username);

        switch (lastUserCommand.LastTelegramCommand)
        {
            case CommandNames.AddExpense:
                {
                    await ExecuteCommand(CommandNames.FinishExpenses, update);
                    break;
                }
            case CommandNames.GetLinkForPurchase:
                {
                    await ExecuteCommand(CommandNames.GetEmail, update);
                    break;
                }
            case CommandNames.GetEmail:
                {
                    await ExecuteCommand(CommandNames.FinishGetLinkForPurchase, update);
                    break;
                }
            case CommandNames.GetStatistic:
                {
                    await ExecuteCommand(CommandNames.FinishStatisticCommand, update);
                    break;
                }
            case null:
                {
                    await ExecuteCommand(CommandNames.NextCommand, update);
                    break;
                }
        }
    }

    private async Task ExecuteCommand(string commandName, Update update)
    {
        _lastCommand = _baseCommands.First(x => x.Name == commandName);
        await _lastCommand.ExecuteAsync(update);
    }
}