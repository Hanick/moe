﻿using Microsoft.AspNetCore.Identity;
using MOE.business.Extensions;
using MOE.data.Entities;
using MOE.data.Repositories;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;
using MOE.sharing.Options;
using Stripe;

namespace MOE.business.Services.Impl;

public class StatisticService : IStatisticService
{
    private readonly IExpenseRepository _expenseRepository;
    private readonly StripeSettings _stripeSettings;
    private readonly ISubscriberRepository _subscriberRepository;
    private readonly UserManager<User> _userManager;

    public StatisticService(IExpenseRepository expenseRepository, StripeSettings stripeSettings, ISubscriberRepository subscriberRepository, UserManager<User> userManager)
    {
        _expenseRepository = expenseRepository;
        _stripeSettings = stripeSettings;
        _subscriberRepository = subscriberRepository;
        _userManager = userManager;
    }

    public StatisticResponse<IQueryable<Expense>> GetStatistic(StatisticRequest statisticRequest, Guid userId)
    {
        StripeConfiguration.ApiKey = _stripeSettings.PrivateKey;

        var previousMonth = DateTime.Now.AddMonths(-1);

        var from = statisticRequest.From;
        var to = statisticRequest.To;

        if (from > to)
        {
            from = statisticRequest.To;
            to = statisticRequest.From;
        }

        var customerId = _userManager.Users.FirstOrDefault(x => x.Id == userId).CustomerId;

        if (customerId != null)
        {
            var subscriptionId = _subscriberRepository.GetSubscriptionId(customerId);

            var stripeService = new SubscriptionService();
            var subscription = stripeService.Get(subscriptionId);

            if (subscription.Status != "active")
            {
                from = 1;
                to = DateTime.DaysInMonth(previousMonth.Year, previousMonth.Month);
            }
        }
        else
        {
            from = 1;
            to = DateTime.DaysInMonth(previousMonth.Year, previousMonth.Month);
        }

        var expenses = _expenseRepository.GetAllUserExpenses(userId, from, to);

        if (!expenses.Any())
            return new StatisticResponse<IQueryable<Expense>>(expenses, 0, 0)
            {
                Errors = new[] { "You don't have any records" },
            };

        expenses = statisticRequest.SortBy.Trim().ToLower() switch
        {
            "check" => statisticRequest.IsAscending ? expenses.OrderBy(x => x.Check) : expenses.OrderByDescending(x => x.Check),
            "totalcheckprice" => statisticRequest.IsAscending ? expenses.OrderBy(x => x.TotalPrice) : expenses.OrderByDescending(x => x.TotalPrice),
            "dateofpurchace" => statisticRequest.IsAscending ? expenses.OrderBy(x => x.DateOfPurchase) : expenses.OrderByDescending(x => x.DateOfPurchase),
            _ => expenses.OrderBy(x => x.DateOfPurchase)
        };

        var totalPrice = expenses.Sum(x => x.TotalPrice);

        return expenses.ToPagedResponse(statisticRequest, totalPrice);
    }
}