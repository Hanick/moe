﻿using MOE.data.Entities;
using MOE.data.Repositories;

namespace MOE.business.Services.Impl;

public class ExpenseService : IExpenseService
{
    private readonly IExpenseRepository _expenseRepository;

    public ExpenseService(IExpenseRepository expenseRepository)
    {
        _expenseRepository = expenseRepository;
    }

    public IQueryable<Expense> GetPosts()
    {
        return _expenseRepository.GetAll();
    }

    public async Task<Expense> GetPostsByIdAsync(Guid expenseId)
    {
        return await _expenseRepository.GetExpenseByIdAsync(expenseId);
    }

    public Task<bool> CreateAsync(Expense postExpense)
    {

        return _expenseRepository.CreateExpenseAsync(postExpense);
    }

    public Task<bool> UpdateAsync(Expense postExpense)
    {
        return _expenseRepository.UpdateExpenseAsync(postExpense);
    }
    public async Task<bool> DeleteAsync(Guid expenseId)
    {
        return await _expenseRepository.SoftDeleteByIdAsync(expenseId, true, DateTime.Now);
    }

    public async Task<bool> UserOwnsExpenseAsync(Guid expenseId, Guid getUserId)
    {
        return await _expenseRepository.UserOwnExpenseAsync(expenseId, getUserId);
    }
}