﻿using MOE.sharing.Options;
using Telegram.Bot;

namespace MOE.business.Services.Impl;

public class BotService : IBotService
{
    private readonly BotSettings _botSettings;
    private TelegramBotClient _telegramBotClient;

    public BotService(BotSettings botSettings)
    {
        _botSettings = botSettings;
    }

    public async Task<TelegramBotClient> GetBot()
    {
        var webHook = $"{_botSettings.WebHook}/api/bot";

        if (_telegramBotClient != null)
        {
            return _telegramBotClient;
        }

        _telegramBotClient = new TelegramBotClient(_botSettings.Token);
        await _telegramBotClient.SetWebhookAsync(webHook);
        return _telegramBotClient;
    }
}