﻿using Telegram.Bot.Types;

namespace MOE.business.Services;

public interface ICommandExecutorService
{
    Task Execute(Update update);
}