﻿using MOE.sharing.DTOs.Domain;
using MOE.sharing.DTOs.Requests;

namespace MOE.business.Services;

public interface IIdentityService
{
    Task<AuthenticationResult> RegisterAsync(RegistrationRequest request);
    Task<AuthenticationResult> LoginAsync(AuthenticationRequest request);
    Task<AuthenticationResult> RefreshTokenASync(string token, string refreshToken);
}