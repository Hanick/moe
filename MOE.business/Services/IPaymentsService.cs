﻿using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;
using Stripe;

namespace MOE.business.Services;

public interface IPaymentsService
{
    Task<CheckoutSessionResponse> CreateCheckoutSession(CheckoutSessionRequest req);
    Task AddSubscriptionToDb(Subscription subscription);
    Task GetCustomerIdAndAddToDb(Customer customer);
    Task UpdateSubscription(Subscription subscription);
}