﻿namespace MOE.business;

public class CommandNames
{
    public const string StartCommand = "/start";
    public const string NextCommand = "/next";
    public const string AddExpense = "/create_expense";
    public const string GetExpenses = "/get_all_expenses";
    public const string GetLinkForPurchase = "/get_premium";
    public const string GetStatistic = "/get_statistic";
    public const string GetPhoneNumber = "/get_phone_number";
    public const string FinishExpenses = "/finish_record_expenses";
    public const string FinishGetLinkForPurchase = "/finish_get_link";
    public const string FinishStatisticCommand = "/finish_statistic";
    public const string GetEmail = "/provide_email";
}