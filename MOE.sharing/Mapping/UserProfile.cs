﻿using AutoMapper;
using MOE.data.Entities;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;

namespace MOE.sharing.Mapping;

public class UserProfile : Profile
{
    public UserProfile()
    {
        CreateMap<CreatePostRequest, Expense>();
        CreateMap<Expense, PostResponse>();
        CreateMap<RegistrationRequest, User>();
    }
}