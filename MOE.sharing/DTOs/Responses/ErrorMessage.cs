﻿namespace MOE.sharing.DTOs.Responses;

public class ErrorMessage
{
    public string? Message { get; set; }
}