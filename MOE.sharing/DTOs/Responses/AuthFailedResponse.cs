﻿namespace MOE.sharing.DTOs.Responses;

public class AuthFailedResponse
{
    public IEnumerable<string>? Errors { get; set; }
}