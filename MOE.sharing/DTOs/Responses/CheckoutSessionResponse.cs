﻿using System.ComponentModel.DataAnnotations;

namespace MOE.sharing.DTOs.Responses;

public class CheckoutSessionResponse
{
    [Required]
    public string? SessionUrl { get; set; }

    public override string ToString()
    {
        return $"\t Link: {SessionUrl}";
    }
}