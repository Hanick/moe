﻿namespace MOE.sharing.DTOs.Responses;

public class ErrorResponse
{
    public ErrorMessage? ErrorMessage { get; set; }
}