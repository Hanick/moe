﻿namespace MOE.sharing.DTOs.Responses;

public class PostResponse
{
    public Guid Id { get; set; }
    public string? Check { get; set; }
    public decimal TotalPrice { get; set; }
    public DateTime DateOfPurchase { get; set; }
    public Guid UserId { get; set; }
}