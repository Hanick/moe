﻿namespace MOE.sharing.DTOs.Responses;

public class StatisticResponse<T> : DataResponse<T>
{
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public int TotalPages { get; set; }
    public int TotalRecords { get; set; }
    public decimal TotalAmount { get; set; }
    public int NumberOfPurchases { get; set; }
    public bool Success { get; set; }
    public IEnumerable<string>? Errors { get; set; }

    public StatisticResponse(T data, int pageNumber, int pageSize)
    {
        PageNumber = pageNumber;
        PageSize = pageSize;
        Data = data;
    }
}