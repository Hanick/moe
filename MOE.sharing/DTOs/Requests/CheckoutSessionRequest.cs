﻿using System.ComponentModel.DataAnnotations;

namespace MOE.sharing.DTOs.Requests;

public class CheckoutSessionRequest
{
    [Required] public string? PriceId { get; set; }
}