﻿namespace MOE.sharing.DTOs.Requests;

public class UpdatePostRequest
{
    public string? Check { get; set; }
    public decimal TotalPrice { get; set; }
    public DateTime DateOfPurchase { get; set; }
}