﻿using MOE.sharing.DTOs.Models;

namespace MOE.sharing.DTOs.Requests;

public class StatisticRequest : PagingModel
{
    public int From { get; set; } 
    public int To { get; set; } 
    public string? SortBy { get; set; } = "DateOfPurchase";
    public bool IsAscending { get; set; }
}