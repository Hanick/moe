﻿using System.ComponentModel.DataAnnotations;

namespace MOE.sharing.DTOs.Requests;

public class RegistrationRequest
{
    public string? UserName { get; set; }
    public string? Password { get; set; }
    [EmailAddress]
    public string? Email { get; set; }
    public string? PhoneNumber { get; set; }
}