﻿namespace MOE.sharing.Options;

public class BotSettings
{
    public string? Token { get; set; }
    public string? WebHook { get; set; }
}