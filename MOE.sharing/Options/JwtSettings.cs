﻿using System.Text.Json.Serialization;

namespace MOE.sharing.Options;

public class JwtSettings
{
    [JsonPropertyName("secret")]
    public string? Secret { get; set; }
    [JsonPropertyName("TokenLifetime")]
    public TimeSpan TokenLifetime { get; set; }
}