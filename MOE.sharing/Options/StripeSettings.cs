﻿namespace MOE.sharing.Options;

public class StripeSettings
{
    public string? PrivateKey { get; set; }
    public string? WHSecret { get; set; }
}