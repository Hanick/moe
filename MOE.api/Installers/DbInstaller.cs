﻿using Microsoft.EntityFrameworkCore;
using MOE.data;
using MOE.data.Entities;
using MOE.data.Repositories;
using MOE.data.Repositories.Impl;

namespace MOE.api.Installers;

public class DbInstaller : IInstaller
{
    public void InstallServices(IServiceCollection service, IConfiguration configuration)
    {
        service.AddDbContext<DataContext>(opt =>
            opt.UseNpgsql(configuration.GetConnectionString("DefaultConnection")));
        service.AddIdentityCore<User>()
            .AddEntityFrameworkStores<DataContext>();

        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

        service.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
        service.AddScoped<IExpenseRepository, ExpenseRepository>();
        service.AddScoped<IStripeRepository, StripeRepository>();
        service.AddScoped<ISubscriberRepository, SubscriberRepository>();
        service.AddScoped<IRefreshTokenRepository, RefreshTokenRepository>();

        service.AddAutoMapper(typeof(Program));
    }
}