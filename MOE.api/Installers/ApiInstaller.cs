﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MOE.business.Commands;
using MOE.business.Services;
using MOE.business.Services.Impl;
using MOE.sharing.Options;
using System.Text;
using MOE.sharing.Mapping;

namespace MOE.api.Installers;
public class ApiInstaller : IInstaller
{
    public void InstallServices(IServiceCollection service, IConfiguration configuration)
    {
        service.AddControllers().AddNewtonsoftJson();

        service.AddScoped<IIdentityService, IdentityService>();
        service.AddScoped<IExpenseService, ExpenseService>();
        service.AddScoped<IStatisticService, StatisticService>();
        service.AddScoped<IPaymentsService, PaymentsService>();
        service.AddScoped<IUserService, UserService>();
        service.AddScoped<ICommandExecutorService, CommandExecutorService>();
        service.AddScoped<IEntitiesService, EntitiesService>();
        service.AddSingleton<IBotService, BotService>();

        service.AddScoped<BaseCommand, StartCommand>();
        service.AddScoped<BaseCommand, NextCommand>();
        service.AddScoped<BaseCommand, AddExpenseCommand>();
        service.AddScoped<BaseCommand, GetExpensesCommand>();
        service.AddScoped<BaseCommand, GetStatisticCommand>();
        service.AddScoped<BaseCommand, GetLinkForPurchaseCommand>();
        service.AddScoped<BaseCommand, GetPhoneNumberCommand>();
        service.AddScoped<BaseCommand, FinishExpensesCommand>();
        service.AddScoped<BaseCommand, FinishGetLinkCommand>();
        service.AddScoped<BaseCommand, FinishStatisticCommand>();
        service.AddScoped<BaseCommand, GetEmailCommand>();

        service.AddAutoMapper(typeof(UserProfile));

        service.AddSingleton(configuration.GetSection("StripeSettings").Get<StripeSettings>());
        service.AddSingleton(configuration.GetSection("BotSettings").Get<BotSettings>());

        service.AddEndpointsApiExplorer();
        var jwtSetting = configuration.GetSection("JwtSettings").Get<JwtSettings>();

        service.AddSingleton(jwtSetting);

        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSetting.Secret)),
            ValidateIssuer = false,
            ValidateAudience = false,
            RequireExpirationTime = false,
            ValidateLifetime = true
        };

        service.AddSingleton(tokenValidationParameters);

        service.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.SaveToken = true;
                x.TokenValidationParameters = tokenValidationParameters;
            });

        service.AddSwaggerGen(x =>
        {
            x.SwaggerDoc("v1", new OpenApiInfo { Title = "MOE api", Version = "v1" });

            var security = new Dictionary<string, IEnumerable<string>>
            {
                    {"Bearer", Array.Empty<string>()}
            };

            x.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Description = "Jwt Authorization header using the bearer scheme",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer"
            });
            x.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
            });
        });
    }
}