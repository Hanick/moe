﻿namespace MOE.api.Installers;

public static class InstallerExtensions
{
    public static void InstallServiceInAssembly(this IServiceCollection service, IConfiguration configuration)
    {
        var installers = typeof(Program).Assembly.ExportedTypes.Where(x =>
                typeof(IInstaller).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract).Select(Activator.CreateInstance)
            .Cast<IInstaller>().ToList();

        installers.ForEach(installer => installer.InstallServices(service, configuration));
    }
}