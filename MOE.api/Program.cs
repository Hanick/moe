using MOE.api.Installers;
using MOE.business.Services;
using MOE.sharing.Options;

var builder = WebApplication.CreateBuilder(args);

builder.Services.InstallServiceInAssembly(builder.Configuration);

var app = builder.Build();
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.Services.GetRequiredService<IBotService>().GetBot().Wait();

var swaggerOptions = new SwaggerOptions();

builder.Configuration.GetSection(nameof(SwaggerOptions)).Bind(swaggerOptions);

app.UseSwagger(options => { options.RouteTemplate = swaggerOptions.JsonRoute; });

app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint(swaggerOptions.UiEndpoint, swaggerOptions.Description);
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();