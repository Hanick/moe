﻿namespace MOE.api.Contracts;

public static class ApiRoutes
{
    public const string Root = "api";
    public const string Version = "v1";
    public const string Base = $"{Root}/{Version}";

    public static class Expense
    {
        public const string Get = Base + "/expense/{expenseId}";
    }
}