﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MOE.api.Contracts;
using MOE.business.Extensions;
using MOE.business.Services;
using MOE.data.Entities;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;

namespace MOE.api.Controllers;

[Authorize]
[ApiController]
[Route("api/post")]
public class ExpenseController : Controller
{
    private readonly IExpenseService _expenseService;
    private readonly IMapper _mapper;
    public ExpenseController(IExpenseService expenseService, IMapper mapper)
    {
        _expenseService = expenseService;
        _mapper = mapper;
    }

    [HttpGet("{expenseId}")]
    public async Task<IActionResult> GetById([FromRoute] Guid expenseId)
    {
        var post = await _expenseService.GetPostsByIdAsync(expenseId);

        if (post == null)
            return BadRequest(new { error = "Expense was not found" });

        return Ok(post);
    }

    [HttpGet("")]
    public async Task<IActionResult> GetAllExpenses()
    {
        return Ok(await _expenseService.GetPosts().ToListAsync());
    }

    [HttpDelete("{expenseId}")]
    public async Task<IActionResult> Delete([FromRoute] Guid expenseId)
    {
        var userOwnExpense = await _expenseService.UserOwnsExpenseAsync(expenseId, new Guid(HttpContext.GetUserId()));

        if (!userOwnExpense)
            return BadRequest(new { error = "You not owner of this expense" });

        var deleted = await _expenseService.DeleteAsync(expenseId);

        if (deleted)
            return NoContent();

        return BadRequest(new { error = "Expense was not found" });
    }

    [HttpPut("{expenseId}")]
    public async Task<IActionResult> Update([FromRoute] Guid expenseId, [FromBody] UpdatePostRequest updatePostRequest)
    {
        var userOwnExpense = await _expenseService.UserOwnsExpenseAsync(expenseId, new Guid(HttpContext.GetUserId()));

        if (!userOwnExpense)
            return BadRequest(new {error = "You not owner of this expense"});

        var post = await _expenseService.GetPostsByIdAsync(expenseId);

        post.Check = updatePostRequest.Check;
        post.TotalPrice = updatePostRequest.TotalPrice;
        post.DateOfPurchase = updatePostRequest.DateOfPurchase;

        var updated = await _expenseService.UpdateAsync(post);

        if (updated)
            return Ok(post);

        return BadRequest(new { error = "Expense was not found"});
    }

    [HttpPost("")]
    public async Task<IActionResult> Create([FromBody] CreatePostRequest request)
    {
        var post = _mapper.Map<Expense>(request);
        post.UserKey = new Guid(HttpContext.GetUserId());

        await _expenseService.CreateAsync(post);

        var baseUrl = $"{HttpContext.Request.Scheme}://{HttpContext.Request.Host.ToUriComponent()}";
        var locationUri = baseUrl + "/" + ApiRoutes.Expense.Get.Replace("{expenseId}", post.Id.ToString());

        var response = _mapper.Map<PostResponse>(post);

        return Created(locationUri, response);
    }
}