﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MOE.business.Extensions;
using MOE.business.Services;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;

namespace MOE.api.Controllers;

[Authorize]
[ApiController]
[Route("api/statistic")]
public class StatisticController : Controller
{
    private readonly IStatisticService _statisticService;

    public StatisticController(IStatisticService statisticService)
    {
        _statisticService = statisticService;
    }

    [HttpGet("")]
    public IActionResult GetStatistic([FromQuery] StatisticRequest statisticRequest)
    {
        var userOwnExpense = _statisticService.GetStatistic(statisticRequest, new Guid(HttpContext.GetUserId()));

        if (!userOwnExpense.Success)
            return BadRequest(new AuthFailedResponse
            {
                Errors = userOwnExpense.Errors
            });

        return Ok(userOwnExpense);
    }
}