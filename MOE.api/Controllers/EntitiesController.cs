﻿using Microsoft.AspNetCore.Mvc;
using MOE.business.Services;

namespace MOE.api.Controllers;

[ApiController]
[Route("api/entities")]
public class EntitiesController : Controller
{
    private readonly IEntitiesService _entitiesService;

    public EntitiesController(IEntitiesService entitiesService)
    {
        _entitiesService = entitiesService;
    }

    [HttpDelete("")]
    public async Task<IActionResult> DeleteAllEntities()
    {
        await _entitiesService.SoftDeleteAllEntities(true, DateTime.Now);

        return NoContent();
    }

    [HttpPut("")]
    public async Task<IActionResult> RestoreAllEntities()
    {
        await _entitiesService.SoftDeleteAllEntities(false, DateTime.UnixEpoch);

        return NoContent();
    }
}