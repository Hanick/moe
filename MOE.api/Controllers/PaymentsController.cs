﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MOE.business.Services;
using MOE.sharing.DTOs.Requests;
using MOE.sharing.DTOs.Responses;
using MOE.sharing.Options;
using Stripe;

namespace MOE.api.Controllers;

[Route("api/payments")]
[ApiController]
public class PaymentsController : Controller
{
    private readonly IPaymentsService _paymentsService;
    private readonly StripeSettings _stripeSettings;

    public PaymentsController(IPaymentsService paymentsService, StripeSettings stripeSettings)
    {
        _paymentsService = paymentsService;
        _stripeSettings = stripeSettings;
    }
    [Authorize]
    [HttpPost("")]
    public async Task<IActionResult> CreateCheckoutSession([FromBody] CheckoutSessionRequest req)
    {
        try
        {
            var res = await _paymentsService.CreateCheckoutSession(req);

            return Ok(new CheckoutSessionResponse
            {
                SessionUrl = res.SessionUrl
            });
        }
        catch (StripeException e)
        {
            return BadRequest(new ErrorResponse
            {
                ErrorMessage = new ErrorMessage
                {
                    Message = e.StripeError.Message,
                }
            });
        }
    }

    [HttpPost("webhook")]
    public async Task<IActionResult> WebHook()
    {
        var json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();

        try
        {
            var stripeEvent = EventUtility.ConstructEvent(
                json,
                Request.Headers["Stripe-Signature"],
                _stripeSettings.WHSecret
            );

            if (stripeEvent.Type == Events.CustomerSubscriptionCreated)
            {
                var subscription = stripeEvent.Data.Object as Subscription;
                await _paymentsService.AddSubscriptionToDb(subscription);

            }
            else if (stripeEvent.Type == Events.CustomerSubscriptionUpdated)
            {
                var session = stripeEvent.Data.Object as Stripe.Subscription;
                await _paymentsService.UpdateSubscription(session);
            }
            else if (stripeEvent.Type == Events.CustomerCreated)
            {
                var customer = stripeEvent.Data.Object as Customer;
                await _paymentsService.GetCustomerIdAndAddToDb(customer);
            }
            else
            {
                Console.WriteLine("Unhandled event type: {0}", stripeEvent.Type);
            }
            return Ok();
        }
        catch (StripeException e)
        {
            Console.WriteLine(e.StripeError.Message);
            return BadRequest();
        }
    }
}