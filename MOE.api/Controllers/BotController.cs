﻿using Microsoft.AspNetCore.Mvc;
using MOE.business.Services;
using Telegram.Bot.Types;

namespace MOE.api.Controllers;

[ApiController]
[Route("api/bot")]
public class BotController : Controller
{
    private readonly ICommandExecutorService _commandExecutorService;

    public BotController(ICommandExecutorService commandExecutorService)
    {
        _commandExecutorService = commandExecutorService;
    }

    [HttpPost("")]
    public async Task<IActionResult> Update([FromBody] Update update)
    {
        if (update.Message?.Chat == null && update.CallbackQuery == null)
        {
            return Ok();
        }

        try
        {
            await _commandExecutorService.Execute(update);
        }
        catch (Exception)
        {
            return Ok();
        }

        return Ok();
    }
}